import { Component, OnInit, Injectable, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'; //FormArray, FormControl
import { Http, Headers } from '@angular/http';
import { User } from '../user.model';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements AfterViewInit {
  formUser: FormGroup;
  post: any;
  username: string = '';
  name: string = '';
  email: string = '';
  city: string = '';
  ride: string = '';
  daysOnWeek: [{}]

  constructor(private formBuilder: FormBuilder, private usersData : User) {}

  ngOnInit() {
    this.formUser = this.formBuilder.group({
      'username' : [null, Validators.compose([Validators.required, Validators.minLength(3)]) ], 
      'name' : [null, Validators.required],
      'email' : [null, Validators.compose([Validators.required, Validators.email])],
      'city' : [null],
      'ride' : [null, Validators.required],
      // 'daysOnWeek' : new FormArray([])
    });
  }

   ngAfterViewInit(){}

  private addUser(post){
    let newUser = {
      'username': post.username,
      'name': post.name,
      'email': post.email,
      'address' : {
        'city' : post.city
      },
      'rideInfo' : {
        'ride' : post.ride,
        'daysOnWeek' : 'none'
      },
      'albums' : [],
      'posts' : [],
    }

    this.usersData.usersList.push(newUser);
    this.resetForm();
  }

  resetForm(){
    this.formUser.reset();
  }
}
