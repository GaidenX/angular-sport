import { Component, OnInit, Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { UsersService } from '../users.service';
import { FormGroup, FormBuilder } from '@angular/forms';

import { User } from '../user.model';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css']
})

export class UserTableComponent implements OnInit {
  constructor(private usersService : UsersService, private usersData : User, private formBuilder: FormBuilder,){}
  
  postsList; albumsList; photosList; rideList;
  searchTable: FormGroup;
  searchInput: string = '';
  
  ngOnInit(){
    this.usersService.requestService('users').subscribe( data =>
      {
          this.usersData.usersList = data;
          this.usersInformation();
      }
    );

    this.searchTable = this.formBuilder.group({
      'search' : [null]
    });
  }

usersInformation(){
  this.usersService.requestService('posts').subscribe( data =>
    {
      this.postsList = data;

      for(var key in this.postsList){
        for(var key2 in this.usersData.usersList){
          if(this.usersData.usersList[key2].id == this.postsList[key].userId){
            if(!Array.isArray(this.usersData.usersList[key2].posts)) this.usersData.usersList[key2].posts = new Array()
            this.usersData.usersList[key2].posts.push(this.postsList[key]);
          }
        }
      }
    }
  );
  
  this.usersService.requestService('rideInfo').subscribe( 
    data =>{
      this.rideList = data;
      
      for(var key in this.usersData.usersList){
        this.usersData.usersList[key].rideInfo = this.rideList[key];
      }
    },
    error => {
      if(error.status == 429) // Usage limit Api
      var data = [
        {
          "id": 1,
          "ride": "always",
          "daysOnWeek" : "Every day"
        },
        {
          "id": 2,
          "ride": "usually",
          "daysOnWeek" : "Week days"
        },
        {
          "id": 3,
          "ride": "normally",
          "daysOnWeek" : "Mon, Wed, Fri"
        },
        {
          "id": 4,
          "ride": "often",
          "daysOnWeek" : "Mon, Tue, Wed"
        },
        {
          "id": 5,
          "ride": "sometimes",
          "daysOnWeek" : "Weekends"
        },
        {
          "id": 6,
          "ride": "occasionally",
          "daysOnWeek" : "Fri, Sun"
        },
        {
          "id": 7,
          "ride": "rarely",
          "daysOnWeek" : "Mon, Wed"
        },
        {
          "id": 8,
          "ride": "never",
          "daysOnWeek" : "Week days"
        },
        {
          "id": 9,
          "ride": "generally",
          "daysOnWeek" : "Every day"
        },
        {
          "id": 10,
          "ride": "frequently",
          "daysOnWeek" : "Tue, Sun"
        }
      ]
      this.rideList = data;
      
      for(var key in this.usersData.usersList){
        this.usersData.usersList[key].rideInfo = this.rideList[key];
      }
    }
  );

  this.albumsInformation();
    // this.usersService.getPhotos().subscribe( data =>
    //   {
    //     this.photosList = data;
    // // for(var key2 in this.photosList){
    // //  if(this.photosList[key2].albumId == this.albumsList[key].id){
    // //     if(!Array.isArray(this.albumsList[key].photos)) this.albumsList[key].photos = new Array()
    // //    this.albumsList[key].photos.push(this.photosList[key2]);
    // //  }

    //     this.albumsInformation();
    //  }
    // );
  }

  albumsInformation(){
    this.usersService.requestService('albums').subscribe( data =>
      {
        this.albumsList = data;

        for(var key in this.albumsList){
      
      // for(var key2 in this.photosList){
      //   if(this.photosList[key2].albumId == this.albumsList[key].id){
      //     if(!Array.isArray(this.albumsList[key].photos)) this.albumsList[key].photos = new Array()
      //     this.albumsList[key].photos.push(this.photosList[key2]);
      //   }
      //}

        for(var key2 in this.usersData.usersList){
          if(this.usersData.usersList[key2].id == this.albumsList[key].userId){
            if(!Array.isArray(this.usersData.usersList[key2].albums)) this.usersData.usersList[key2].albums = new Array()
            this.usersData.usersList[key2].albums.push(this.albumsList[key]);
          }
        }
      }
    });
  }

  deleteItem(i) {
    if(confirm("Are you sure to delete?")) {
      this.usersData.usersList.splice(i,1);
    }
  }

  //Search Table
  searchUser(searchTerm){
    this.searchInput = (searchTerm.search).toLowerCase();
    console.log(this.searchInput);
  }
}