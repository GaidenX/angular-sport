import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class UsersService {
  constructor(private http: HttpClient) {}

// Url's services
private urlUsers = 'https://jsonplaceholder.typicode.com/users';
private postsUsers = 'https://jsonplaceholder.typicode.com/posts';
private albumsUsers = 'https://jsonplaceholder.typicode.com/albums';
private photosUsers = 'https://jsonplaceholder.typicode.com/photos';
private rideUsers =  'https://9a3b73a6-f278-4b74-a799-c4714ad88f4a.mock.pstmn.io/usersinfo/';
private dropdownConfig =  'https://9a3b73a6-f278-4b74-a799-c4714ad88f4a.mock.pstmn.io/dropdown/';


// Header Service
httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

// Simple request url
requestService(service){
    if(service == "users") return this.http.get(this.urlUsers);
    else if(service == "posts") return this.http.get(this.postsUsers);
    else if(service == "albums") return this.http.get(this.albumsUsers);
    else if(service == "photos") return this.http.get(this.photosUsers);
    else if(service == "rideInfo") return this.http.get(this.rideUsers);
    else if(service == "dropDown") return this.http.get(this.dropdownConfig);
}
}
