import { Component, OnInit, Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { UsersService } from './users.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Venturus Sports';
  dropdownItem : any ={
    userSession : '',
    itemsList : [],
    logOut : ''
  };

  constructor(private configDropdown : UsersService){}

  ngOnInit(){
    this.configDropdown.requestService('dropDown').subscribe( 
      data => {
        this.dropdownItem = data;
      },
      error => {
        if(error.status == 429) // Usage limit Api
        var data = {
          "userSession": "Caio Americo",
          "itemsList": [
              "Friends List",
              "Saved Items",
              "Notifications",
              "User Preferences"
          ],
          "logOut": "Log out"
      }
      this.dropdownItem = data
      }
    );
  }
}
